> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web Application Development

## Michael Birdsong 

### Assignment 1 Requirements:

*Sub-Heading:*

1. AMPPS
2. JAVA
3. Android Studio

#### README.md file should include the following items:

* screenshot of AMPPS installation running
* screenshot of JDK java Hello
* screenshot of running Android Studio My First App
* Git commands definitions 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. get Int: Initializes a git repository
2. get status: List the files you've changed and those you still need to add or commit
3. git add: Add one or more files to staging
4. git comment: Commit changes to head, but not yet to the remote repository
5. git push: Send changes to the master branch of your remote repository
6. git pull: Fetches the files from the remote repository and merges it with your local one.
7. git remote: Shows all the remote versions of your repository

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

![AMPPS Installation Screenshot](img/ampps2.png)

![AMPPS Installation Screenshot](img/ampps3.png)


*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mab12b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/mab12b/myteamquotes/ "My Team Quotes Tutorial")
