> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Applications 

## Michael Birdsong 

### Assignment 4 Requirements:


1. Provide Bitbucket read-only access to lis 4381 repo, include links to the repos you
   created in the above tutorials in README.md, using Markdown syntax
   (README.md must also include screenshots as per above.)
2. Blackboard Links:lis 4381 Bitbucket repo
3. *Note*:the carousel *must* contain(min.3) slides that either contain text or
   images that link to other content areas marketing/promoting your skills.

#### README.md file should include the following items:

* Course title, yourname, assignment requirements, as per A1; 
* Screen shots as per below examples;
* Link to local lis 4381 webapp:http://localhost/repos/lis4381/


#### Assignment Screenshots:

*Screenshot of Main Page*:

![AMPPS Installation Screenshot](img/MainPage.png)

*Screenshot of Failed Invalid Page*:

![JDK Installation Screenshot](img/FailedValidation.png)

*Screenshot of Passed Validation Page - My First App*:

![Android Studio Installation Screenshot](img/PassedValidation.png)


