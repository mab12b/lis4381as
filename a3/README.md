> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis 4381 Mobile Applications

## Michael Birdsong 

### Assignment 3 Requirements:

*Sub-Heading:*

1. *All* tables must have notes.
2. Include *suitable* data in the MWB file, at least 10 “unique” records per table, 
**must forward-engineer**! 
3. *Be sure* to match data types.

#### README.md file should include the following items:


* ERD created
* having data (min. 10 records per table) 



#### Assignment Screenshots:

*Screenshot of ERD running*:

![MySQL Installation Screenshot](img/MySQL.png)

![MySQL Tables](img/slt.png)

![MySQL select statements](img/slt2.png)

*A3 ERD MWB file*:
[A3 in lis4381.mwb format](docs/lis4381a3.mwb "Link to my MWB")

*A3 SQL file*:
[A3 in a3.sql format](docs/a3.sql "Link to my .sql")



