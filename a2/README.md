> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis 4381 Mobile Applications

## Michael Birdsong 

### Assignment 2 Requirements:

*Sub-Heading:*

1. Screenshot of running application’s first user interface
2. Screen shot of running application’s second user interface

#### README.md file should include the following items:



#### Assignment Screenshots:

*Screenshot of Healthy Recipe*:

![Healthy Recipe Screenshot](img/HR.png)

*Screenshot of running java Hello*:

![ Healthy Recipe Screenshot](img/HR2.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Healthy Recipe Screenshot](img/HR3.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mab12b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/mab12b/myteamquotes/ "My Team Quotes Tutorial")
