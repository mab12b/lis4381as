> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Applications

## Michael Birdsong 

### Assignments: A1, A2, A3, A4, P1, A5, P2


1. [A1 README File ](a1/README.md) "My A1 README File"

* screenshot of AMPPS installation running
* screenshot of JDK java Hello
* screenshot of running Android Studio My First App
* Git commands definitions

2. [A2 README File ](a2/README.md) "MY A2 README File"

* Screenshot of running application’s first user interface
* Screen shot of running application’s second user interface

3. [A3 README File ](a3/README.md) "My A3 README File"

* Alltables must have notes.
* Include *suitable* data in the MWB file, at least 10 “unique” records per table, 
* *must forward-engineer**! 
* Be sure* to match data types.
* ERD created
* having data (min. 10 records per table) 

4. [P1 README File ](p1/README.md) "My P1 README File"

*  Course title, yourname, assignment requirements, as per A1; 
*  Screenshot of running application’s first user interface;
*  Screenshot of running application’s second user interface;

5. [A4 README File ](a4/README.md) "My A4 README File"

* Course title, yourname, assignment requirements, as per A1; 
* Screen shots as per below Main Page, Failed Validation, Passed Validation;
* Link to local lis 4381 webapp:http://localhost/repos/lis4381/

6. [A5 README File ](a5/README.md) "My A5 README File"

* Course title, yourname, assignment requirements, as per A1; 
* Screen shots as per below Index.php and add_petstore_process;
* Link to local lis 4381 webapp:http://localhost/repos/lis4381/

7. [P2 README File ](p2/README.md) "My P2 README File"

* Course title, yourname, assignment requirements, as per A1; 
* Screen shots as per below p2Index.php and rssfeed.php;
* Link to local lis 4381 webapp:http://localhost/repos/lis4381/

