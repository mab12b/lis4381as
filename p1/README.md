> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis 4381 Mobile Applications

## Michael Birdsong 

### Project 1 Requirements:

*Sub-Heading:*

1. Create a launcher icon image and display it in both activities (screens)
2. Must add background color(s) to both activities
3. Must add border around image and button
4. Must add text shadow (button)

#### README.md file should include the following items:

*  Course title, yourname, assignment requirements, as per A1; 
*  Screenshot of running application’s first user interface;
*  Screenshot of running application’s second user interface;


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Front of app*:

![Font Screenshot](img/front.png)

*Screenshot of Details*:

![Details Screenshot](img/details.png)

*Screenshot of Main activity*:

![Main activity Screenshot](img/activitymain.png)

